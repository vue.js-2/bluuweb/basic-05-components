Vue.component("contador", {
  template: `
      <div>
        <h2>{{contador}}</h2>
        <button v-on:click="contador++">+</button>
      </div>
    `,
  data() {
    return {
      contador: 0,
    };
  },
});
